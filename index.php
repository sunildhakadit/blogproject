<?php include("include/header.php"); ?>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
   
    <div class="hero-area">
            
        <!-- Hero Slides Area -->
        <div class="hero-slides owl-carousel  h-200">
               
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img" id="height" style="background-image: url(img/bg-img/b2.jpg); ">
                
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="slide-content text-center">
                                <div class="post-tag">
                                    <a href="#" data-animation="fadeInUp">lifestyle</a>
                                </div>
                                <h2 data-animation="fadeInUp" data-delay="250ms" id="slider-heading"><a href="single-post.html">Take a look at last night’s party!</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img" id="height" style="background-image: url(img/bg-img/b1.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="slide-content text-center">
                                <div class="post-tag">
                                    <a href="#" data-animation="fadeInUp">lifestyle</a>
                                </div>
                                <h2 data-animation="fadeInUp" data-delay="250ms"  id="slider-heading"><a href="single-post.html">Take a look at last night’s party!</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img" id="height" style="background-image: url(img/bg-img/b3.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="slide-content text-center">
                                <div class="post-tag">
                                    <a href="#" data-animation="fadeInUp">lifestyle</a>
                                </div>
                                <h2 data-animation="fadeInUp" data-delay="250ms"  id="slider-heading"><a href="single-post.html">Take a look at last night’s party!</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->
 <br>
    <!-- <h2 class="text-center" style="font-family: Lucida Sans Unicode;" >Category</h2> -->
    <div class="space4"></div>    

<div class="tab" style="margin-left:100px;">
     <!-- ##### Instagram Feed Area Start ##### -->
     <div class="instagram-slides owl-carousel">
        <!-- Single Insta Feed -->
        <div class="single-insta-feed" id="Category-box" style="margin:20px;">
                <a class="btn tablinks" id="demo" onclick="openCity(event, 'TIPS')">
                 
                    <div class="row">
                      <div class="col-5"> 
                  
                      </div>
                    <div class="col-7">
                      <p class="london">TIPS</p>
                    </div>
                </div>
                    
                     </a>
                     <img src="img/bg-img/b2.jpg" alt=""  style="width:60px;" class=" circle-img">
              </div>

              <div class="single-insta-feed" id="Category-box" style="margin:20px;">
                <a class="btn tablinks" id="demo" onclick="openCity(event, 'TRICKS')">
                 
                    <div class="row">
                      <div class="col-5"> 
                  
                      </div>
                    <div class="col-7">
                      <p class="london">TRICKS</p>
                    </div>
                </div>
                    
                     </a>
                     <img src="img/bg-img/b2.jpg" alt=""  style="width:60px;" class=" circle-img">
              </div>
              <div class="single-insta-feed" id="Category-box" style="margin:20px;">
                <a class="btn tablinks" id="demo" onclick="openCity(event, 'OFFERS')">
                 
                    <div class="row">
                      <div class="col-5"> 
                  
                      </div>
                    <div class="col-7">
                      <p class="london">OFFERS</p>
                    </div>
                </div>
                    
                     </a>
                     <img src="img/bg-img/b2.jpg" alt=""  style="width:60px;" class=" circle-img">
              </div>
              <div class="single-insta-feed" id="Category-box" style="margin:20px;">
                <a class="btn tablinks" id="demo" onclick="openCity(event, 'REFFEREARN')">
                 
                    <div class="row">
                      <div class="col-3"> 
                  
                      </div>
                    <div class="col-9">
                      <p class="london">REFFER $ EARN</p>
                    </div>
                </div>
                    
                     </a>
                     <img src="img/bg-img/b2.jpg" alt=""  style="width:60px;" class=" circle-img">
              </div>
        <!-- Single Insta Feed -->
        <div class="single-insta-feed" id="Category-box" style="margin:20px;">
                <a class="btn tablinks" id="demo" onclick="openCity(event, 'SOLUTION')" >
                 
                    <div class="row">
                      <div class="col-5"> </div>
                    <div class="col-7">
                            <p class="london">SOLUTION</p>
                    </div></div>
                    
                     </a>
                     <img src="img/bg-img/b2.jpg" alt=""  style="width:60px;" class=" circle-img">
              </div>

      </div>
      
  <div id="TIPS" class="tabcontent">
     
     <div class="blog-wrapper  clearfix">
        <div class="container">
            <div class="row align-items-end" id="india" class="tabcontent">
                <!-- Single Blog Area -->
                          
               <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
      //$query = " SELECT * FROM tb_blogs";
      $query="select * from `tb_blogs` where categoryName='TIPS'";
	  $data = mysqli_query($config,$query);
	  $total = mysqli_num_rows($data);
	
	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100 card first" style="height: 30rem;">
                    <img class="card-img-top" src="<?php echo $result["image"]; ?>">
                        <!-- Blog Content -->
                        <div class="card-body" style="overflow: hidden;">
                            <div class="line"></div>
                            <h4><?php echo $result['name'] ?></h4>
                                <p><?php echo $result['editer'] ?></p>
                           
                            </div>
                                                          
                             <div class="end">
                             <a href="readmore.php" class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Read More</a>
                             <a href='<?php echo $result['actionurl'] ?>' target="_blank"  class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Let's Begin</a>
                            
                        </div>
                    </div>
                </div>
                <?php }}?>
</div> 
</div>
 </div>
  </div>

       
  <div id="TRICKS" class="tabcontent">
     
     <div class="blog-wrapper  clearfix">
        <div class="container">
            <div class="row align-items-end" id="TRICKS" class="tabcontent">
                <!-- Single Blog Area -->
                          
               <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
      
      $query="select * from `tb_blogs` where categoryName='TRICKS'";
	  $data = mysqli_query($config,$query);
	  $total = mysqli_num_rows($data);
	
	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100 card first" style="height: 30rem;">
                    <img class="card-img-top" src="<?php echo $result["image"]; ?>">
                        <!-- Blog Content -->
                        <div class="card-body" style="overflow: hidden;">
                            <div class="line"></div>
                            <h4><?php echo $result['name'] ?></h4>
                                <p><?php echo $result['editer'] ?></p>
                           
                            </div>
                                                          
                             <div class="end">
                             <a href="readmore.php" class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Read More</a>
                             <a href='<?php echo $result['actionurl'] ?>' target="_blank"  class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Let's Begin</a>
                            
                        </div>
                    </div>
                </div>
                <?php }}?>
</div> 
</div>
 </div>
  </div>
  
       
  <div id="OFFERS" class="tabcontent">
     
     <div class="blog-wrapper  clearfix">
        <div class="container">
            <div class="row align-items-end" id="OFFERS" class="tabcontent">
                <!-- Single Blog Area -->
                          
               <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
     
      $query="select * from `tb_blogs` where categoryName='OFFERS'";
	  $data = mysqli_query($config,$query);
	  $total = mysqli_num_rows($data);
	
	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100 card first" style="height: 30rem;">
                    <img class="card-img-top" src="<?php echo $result["image"]; ?>">
                        <!-- Blog Content -->
                        <div class="card-body" style="overflow: hidden;">
                            <div class="line"></div>
                            <h4><?php echo $result['name'] ?></h4>
                                <p><?php echo $result['editer'] ?></p>
                           
                            </div>
                                                          
                             <div class="end">
                             <a href="readmore.php" class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Read More</a>
                             <a href='<?php echo $result['actionurl'] ?>' target="_blank"  class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Let's Begin</a>
                            
                        </div>
                    </div>
                </div>
                <?php }}?>
</div> 
</div>
 </div>
  </div>
  
       
  <div id="REFFEREARN" class="tabcontent">
     
     <div class="blog-wrapper  clearfix">
        <div class="container">
            <div class="row align-items-end" id="REFFEREARN" class="tabcontent">
                <!-- Single Blog Area -->
                          
               <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
  
      $query="select * from `tb_blogs` where categoryName='REFFER $ EARN'";
	  $data = mysqli_query($config,$query);
	  $total = mysqli_num_rows($data);
	
	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100 card first" style="height: 30rem;">
                    <img class="card-img-top" src="<?php echo $result["image"]; ?>">
                        <!-- Blog Content -->
                        <div class="card-body" style="overflow: hidden;">
                            <div class="line"></div>
                            <h4><?php echo $result['name'] ?></h4>
                                <p><?php echo $result['editer'] ?></p>
                           
                            </div>
                                                          
                             <div class="end">
                             <a href="readmore.php" class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Read More</a>
                             <a href='<?php echo $result['actionurl'] ?>' target="_blank"  class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Let's Begin</a>
                            
                        </div>
                    </div>
                </div>
                <?php }}?>
</div> 
</div>
 </div>
  </div>
  
  
  <div id="SOLUTION" class="tabcontent">
        <div class="blog-wrapper  clearfix">
           <div class="container">
           <div class="row align-items-end" id="SOLUTION" class="tabcontent">
             
                <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
   
      $query="select * from `tb_blogs` where categoryName='SOLUTION'";
	  $data = mysqli_query($config,$query);
	  $total = mysqli_num_rows($data);
	
	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                          
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100 card first" style="height: 30rem;">
                    <img class="card-img-top" src="<?php echo $result["image"]; ?>">
                        <!-- Blog Content -->
                        <div class="card-body" style="overflow: hidden;">
                            <div class="line"></div>
                        
                            <h4><?php echo $result['name'] ?></h4>
                                <p><?php echo $result['editer'] ?></p>
                            </div>
                            
                                                                  
                             <div class="end">
                             <a href="readmore.php" class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Read More</a>
                             <a href='<?php echo $result['actionurl'] ?>' target="_blank"  class="btn read1" style="float:left;margin-bottom:10px; margin-top:10px; margin-left:15px;">Let's Begin</a>
                            
                        </div>
                    </div>
                </div>
                <?php }}?>
</div>    
                       
                   </div>
           </div>
           </div>
              
                   </div>  
                </div>   
   
  
    
  <script>
        function openCity(evt, cityName) {
          var i, tabcontent, tablinks;
          tabcontent = document.getElementsByClassName("tabcontent");
          for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablinks");
          for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
          }


          document.getElementById(cityName).style.display = "block";
          evt.currentTarget.className += " active";
        }
        // Add active class to the current button (highlight it)
var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("tablinks");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
 document.getElementsByClassName("active").style.color = "white";
  current[0].className = current[0].className.replace(" active", "");
  
  this.className += " active";
  });
}
        </script>
    <!-- ##### Blog Wrapper Start ##### -->
  

        <div class="pagination" >
                <a href="#">&laquo;</a>
                <a href="#" class="active">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
           
                <a href="#">&raquo;</a>
              </div>
              

   

    <?php include("include/footer.php");?>