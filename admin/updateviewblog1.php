<?PHP include("include/header.php"); ?>        
<?PHP include("include/leftmenuheader.php"); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Form Elements</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="updateviewblogNow.php" method="post" enctype="multipart/form-data"  data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="hidden" name="id" readonly value="<?php echo $_GET['rid']; ?>"    class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Name 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="name" class="form-control col-md-7 col-xs-12" id="first-name" value="<?php echo $_GET['rn']; ?>"/>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> select Category
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select  name="categoryName">
                          <option value=" <?php echo $_GET['rcn']; ?>">..<?php echo $_GET['rcn']; ?>..</option>
                           
                          <?php include("connection.php");
   error_reporting(0);
 ?>
                <?php 
     
      $query="select * from `tbc_category` order by id desc";
      $data = mysqli_query($config,$query);
      $total = mysqli_num_rows($data);

	  if($total != 0)
	  {

		 while($result = mysqli_fetch_assoc($data))
         {?>
                          
                            <option value="<?php echo $result["cname"]; ?>"><?php echo $result["cname"]; ?></option>
                           
                <?php }}?>
                         
                          </select>
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> preference <span class="required">*</span>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select  name="preference">
                          <option value="select">..select..</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" > uploadimage 
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                        
                       <label> <input type="file" name="uploadfile"  value="<?php echo $_GET['ri']; ?>"/></label>
                        <?php echo $_GET['ri']; ?>
                      
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Action Url <span class="required">*</span>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                        
                        <input type="text" name="actionurl" value="<?php echo $_GET['ral']; ?>" class="form-control col-md-7 col-xs-12"/>
                      
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Category Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="ckeditor" id="content" name="editer"><?php echo $_GET['re']; ?></textarea>
                          
                        </div>
                      </div>
                     
                  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <input type="submit" name="submit" value="Submit" class="btn btn-success"/>
                         
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->
        <script src="ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editer', {
        filebrowserUploadUrl: 'ckeditor/ck_upload.php',
        filebrowserUploadMethod: 'form'
    });
</script>

        <?PHP include("include/footer.php"); ?> 
        <!-- footer content -->
      