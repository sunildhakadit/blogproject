<!-- ##### Footer Area Start ##### -->
<footer class="footer-area " style="background-color: gainsboro;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <ul>
                            <li><b>CONTACT US</b></li>
                            <br>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i >&nbsp; Plot No- 50, 2nd Floor, Behind Bawa Timber, M.B Road Saidulajab , New Delhi -110030
                           </li>
                           <br>
                           <li><i class="fa fa-volume-control-phone" aria-hidden="true"></i>&nbsp; +91 9818401401</li>
                           <br>
                           <li><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; suddhir@liftmobi.com</li>
                        </ul>
                    </div>
               
                    <div class="col-md-2">
    
                        <ul >
                            <li><b>NAVIGATION</b></li>
                            <br>
                            <a href="tips.html"><li>Tips</li></a>
                            <br>
                            <a href="tricks.html"><li>Tricks</li></a>
                            <br>
                            <a href="offers.html"><li>Offers</li></a>
                            <br>
                            <a href="reffer and earn.html"> <li>Refer & Earn</li></a>
                            <br>
                            <a href="solution.html"><li>Solution</li></a>
                        </ul>
                    </div>
                    <div class="col-md-2 ">
                            <ul >
                                <li><b>OTHER LINKS</b></li>
                                <br>
                                <a href="index.html"><li>Home</li></a>
                                <br>
                                <a href="about-us.html"><li>About</li></a>
                                <br>
                                <a href="contact.html"><li>Contact</li></a>
                                <br>
                               
                                
                            </ul>
                        </div>
                        <div class="col-md-4"><form action="#" method="post">
    
                           <ul class=""><li><b>SUBSCRIBE TO STAY UP TO DATE WITH THE LATEST STORIES ON CONVERSATIONAL EXPERIENCES</b></li></ul>
                           <br>
                           <div class="row ">
                                <div class="col-md-2">  <label>Name</label></div>
                                <div class="col-md-10"><input type="Name" class="name" placeholder="FullName"></div>
                               <div class="col-md-2">  <label>Email</label></div>
                               <div class="col-md-10"><input type="email" class="name" placeholder="example@gmail.com"></div>
                               <div class="col-md-2">  </div>
                               <div class="col-md-10"><a href="" class="btn " id="submit">Subscribe</a></div>
                           </div>
                         
                        </form></div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                       
                        
                        <!-- Footer Social Area -->
                        <div class="footer-social-area mt-30">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Whatsapp"><i class="fa fa-whatsapp" id="one" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" id="one" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" id="one" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" id="one" aria-hidden="true"></i></a>
                      
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin" id="one" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
    
       <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
           <p class="text-center">Copyright &copy; <script>document.write(new Date().getFullYear());</script>. All rights reserved  </p>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    
        </footer>
    
        <!-- ##### Footer Area End ##### -->
    <!-- ##### Footer Area End ##### -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
   
</body>

</html>